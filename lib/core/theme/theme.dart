import 'package:flutter/material.dart';

import '../colorPalette.dart';

class AppTheme {
  static ThemeData dark() {
    var themeData = ThemeData(
      primarySwatch: generateMaterialColor(Palette.PRIMARY),
      primaryColor: Palette.PRIMARY,
      hintColor: Palette.DARK,
      accentColor: Palette.ACCENT,
      fontFamily: 'MainFont',
      brightness: Brightness.dark,
      iconTheme: IconThemeData(color: Palette.PRIMARY),
      textTheme: TextTheme(
        headline3: TextStyle(
          fontFamily: 'AdditionalFont',
          color: Palette.DARK,
        ),
      ),
      appBarTheme: AppBarTheme(
        color: Colors.transparent,
        brightness: Brightness.dark,
        elevation: 0,
        iconTheme: IconThemeData(color: Palette.PRIMARY),
        textTheme: TextTheme(
          headline4: TextStyle(color: Palette.PRIMARY, fontFamily: 'MainFont'),
        ),
      ),
    );
    return themeData;
  }
}
