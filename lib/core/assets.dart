class Assets {
  static const PERSON = "lib/core/assets/images/person.png";
  static const ANDROID = "lib/core/assets/images/android.png";
  static const NOTIFY = "lib/core/assets/images/notification.png";
  static const RIDERS = 'lib/core/assets/images/riders.png';

  static const FARCRY = 'lib/core/assets/images/farcry.png';
  static const ACTION = "lib/core/assets/images/action.png";
  static const RED = "lib/core/assets/images/red.png";
  static const BLUE = "lib/core/assets/images/blue.png";
  static const AWHITE = "lib/core/assets/images/Awhite.png";
  static const GREEN = "lib/core/assets/images/green.png";
  static const WHITE = "lib/core/assets/images/white.png";
  static const REDBLACK = "lib/core/assets/images/redBlack.png";
}
