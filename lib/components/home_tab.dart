import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Assignment/presentation/screens/homeScreen.dart';

class Hometab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<Hometab> {
  int _selectedTab = 0;
  final _pageOptions = [
    HomePageScreen(),
    HomePageScreen(),
    HomePageScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedTab,
        onTap: (int index) {
          setState(() {
            _selectedTab = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.list,
            ),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.assignment,
            ),
            label: "",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.favorite_border_outlined,
            ),
            label: "",
          ),
        ],
      ),
      body: _pageOptions[_selectedTab],
    ));
  }
}
