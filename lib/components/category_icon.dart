import 'package:Assignment/core/colorPalette.dart';
import 'package:Assignment/model/category_icon_model.dart';

import 'package:Assignment/model/sorting_model.dart';

import 'package:flutter/material.dart';

class CategoryIconButton extends StatefulWidget {
  final CategoryIconModel categoryIconModel;
  final Function onTap;
  const CategoryIconButton({Key key, this.categoryIconModel, this.onTap})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<CategoryIconButton> {
  bool category = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 6.2),
      child: OutlineButton.icon(
          //height: 42,
          splashColor: Palette.SUCCESS,
          borderSide: BorderSide(
            width: 3,
            color: category ? Palette.Grey : Palette.SECONDARY,
          ),
          onPressed: () {
            setState(
              () => category = !category,
            );
          },
          label: Text(
            widget.categoryIconModel.category,
            style: TextStyle(fontSize: 10),
          ),
          icon: Icon(
            widget.categoryIconModel.iconData,
            color: category ? Palette.Grey : widget.categoryIconModel.color,
          ),
          shape: RoundedRectangleBorder(
              // side: BorderSide(
              //   color: Palette.PRIMARY,
              //   width: 2,
              //   style: BorderStyle.solid,
              // ),
              borderRadius: BorderRadius.circular(22))),
    );
  }
}
