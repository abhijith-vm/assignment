import 'package:Assignment/core/assets.dart';
import 'package:Assignment/model/game_model.dart';
import 'package:flutter/material.dart';

class GamesCard extends StatefulWidget {
  final GameModel gameModel;
  GamesCard({Key key, this.gameModel}) : super(key: key);

  @override
  _GamesCardState createState() => _GamesCardState();
}

class _GamesCardState extends State<GamesCard> {
  bool fav = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 242,
      margin: EdgeInsets.fromLTRB(24, 8, 24, 12),

      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                topRight: Radius.circular(8.0),
                bottomLeft: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0))),
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  child: Image(
                    image: AssetImage(widget.gameModel.prflImg),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(widget.gameModel.name),
                    SizedBox(
                      width: 68,
                    ),
                    IconButton(
                      icon: fav
                          ? Icon(Icons.favorite_border)
                          : Icon(Icons.favorite),
                      onPressed: () {
                        setState(
                          () => fav = !fav,
                        );
                      },
                    )
                  ],
                )
              ],
            )),
      ),
    );
  }
}
