import 'package:Assignment/core/colorPalette.dart';
import 'package:Assignment/model/sorting_model.dart';
import 'package:flutter/material.dart';

class CustomFlatIconButton extends StatelessWidget {
  final Function onTap;

  final SortingModel sortingModel;
  const CustomFlatIconButton({
    Key key,
    this.onTap,
    this.sortingModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      splashColor: Palette.PRIMARY,
      color: Palette.DARK,
      onPressed: () {
        onTap();
      },
      label: Text(
        sortingModel.category,
        style: TextStyle(fontSize: 10),
      ),
      shape: RoundedRectangleBorder(
          // side: BorderSide(
          //   color: Palette.PRIMARY,
          //   width: 2,
          //   style: BorderStyle.solid,
          // ),
          borderRadius: BorderRadius.circular(22)),
      icon: sortingModel.iconData != null
          ? Icon(
              sortingModel.iconData,
              size: 14,
              color: sortingModel.color,
            )
          : Container(),
    );
  }
}
