import 'package:Assignment/components/customFlat_icon_button.dart';
import 'package:Assignment/core/assets.dart';
import 'package:Assignment/core/colorPalette.dart';
import 'package:Assignment/model/game_model.dart';
import 'package:Assignment/model/sorting_model.dart';
import 'package:flutter/material.dart';

class TodayGamesCard extends StatefulWidget {
  final GameModel gameModel;
  final SortingModel sortingModel;
  @required
  final String prflImg;
  @required
  final String name;
  TodayGamesCard(
      {Key key, this.gameModel, this.sortingModel, this.prflImg, this.name})
      : super(key: key);

  @override
  _TodayGamesCardState createState() => _TodayGamesCardState();
}

class _TodayGamesCardState extends State<TodayGamesCard> {
  bool fav = true;
  @override
  Widget build(BuildContext context) {
    final sortingList = sortingModel;
    int sortingcount = sortingList.length;
    return Container(
      // height: 242,
      margin: EdgeInsets.fromLTRB(24, 8, 24, 12),

      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                topRight: Radius.circular(8.0),
                bottomLeft: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0))),
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  child: Image(
                    image: AssetImage(
                      widget.prflImg,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(widget.name),
                    SizedBox(
                      width: 68,
                    ),
                    IconButton(
                      icon: fav
                          ? Icon(Icons.favorite_border)
                          : Icon(Icons.favorite),
                      onPressed: () {
                        setState(
                          () => fav = !fav,
                        );
                      },
                    )
                  ],
                ),
                GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: sortingcount,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 2.6,
                        crossAxisCount: 3,
                        crossAxisSpacing: 4,
                        mainAxisSpacing: 8.0),
                    itemBuilder: (BuildContext context, int i) {
                      return CustomFlatIconButton(
                        sortingModel: sortingList[i],
                        onTap: () {},
                      );
                    }),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "download",
                        style: TextStyle(color: Palette.Grey),
                      ),
                    ),
                    buildDownload(Assets.ANDROID),
                    buildDownload(Assets.AWHITE),
                    buildDownload(Assets.BLUE),
                    buildDownload(Assets.RED),
                    buildDownload(Assets.WHITE),
                    buildDownload(Assets.REDBLACK),
                    //  buildDownload(Assets.GREEN)
                  ],
                ),
              ],
            )),
      ),
    );
  }

  Widget buildDownload(String assetName) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.only(left: 3),
        child: Image(
          width: 28,
          height: 28,
          image: AssetImage(assetName),
        ),
      ),
    );
  }
}
