import 'package:Assignment/core/colorPalette.dart';
import 'package:Assignment/model/category_model.dart';

import 'package:flutter/material.dart';

class CustomCategoryButton extends StatefulWidget {
  final CategoryModel categorymodel;
  final Function onTap;
  const CustomCategoryButton({Key key, this.categorymodel, this.onTap})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<CustomCategoryButton> {
  bool category = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 6.2),
      child: FlatButton(
          splashColor: Palette.SUCCESS,
          color: category ? Palette.DARK : Palette.SECONDARY,
          onPressed: () {
            setState(
              () => category = !category,
            );
          },
          child: Text(
            widget.categorymodel.category,
            style: TextStyle(fontSize: 10),
          ),
          shape: RoundedRectangleBorder(
              // side: BorderSide(
              //   color: Palette.PRIMARY,
              //   width: 2,
              //   style: BorderStyle.solid,
              // ),
              borderRadius: BorderRadius.circular(22))),
    );
  }
}
