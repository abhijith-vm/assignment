class GameModel {
  String name;
  String number;
  String prflImg;

  GameModel({
    this.number,
    this.name,
    this.prflImg,
  });
}

List<GameModel> gameModel = [
  GameModel(
    name: 'Farcry',
    number: '6',
    prflImg: "lib/core/assets/images/farcry.png",
  ),
  GameModel(
    name: 'Riders republic',
    number: '8',
    prflImg: "lib/core/assets/images/riders.png",
  )
];
