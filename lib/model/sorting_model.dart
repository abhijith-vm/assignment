import 'package:Assignment/core/colorPalette.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SortingModel {
  String category;
  IconData iconData;
  Color color;
  SortingModel({this.category, this.iconData, this.color});
}

List<SortingModel> sortingModel = [
  SortingModel(
    category: "Action",
    iconData: Icons.flash_on,
    color: Colors.orangeAccent,
  ),
  SortingModel(
      category: "Advent",
      iconData: (Icons.flash_on_outlined),
      color: Colors.blueGrey),
  SortingModel(
    category: "12 feb",
    iconData: Icons.calendar_today_outlined,
    color: Palette.ACCENT,
  ),
  SortingModel(
    category: "30% off",
    iconData: Icons.label_outlined,
    color: Colors.yellow,
  ),
];
