import 'package:Assignment/core/colorPalette.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryIconModel {
  String category;
  IconData iconData;
  Color color;
  CategoryIconModel({this.category, this.iconData, this.color});
}

List<CategoryIconModel> categoryIconModel = [
  CategoryIconModel(
    category: "All",
    iconData: Icons.flash_on,
    color: Colors.orangeAccent,
  ),
  CategoryIconModel(
    category: "Puzzle",
    iconData: Icons.web,
    color: Palette.ACCENT,
  ),
  CategoryIconModel(
      category: "Adventure",
      iconData: (Icons.flash_on_outlined),
      color: Colors.red),
  CategoryIconModel(
    category: "Free offer",
    iconData: Icons.label_outlined,
    color: Colors.yellow,
  ),
  CategoryIconModel(
    category: "Action",
    iconData: Icons.flash_on,
    color: Colors.orangeAccent,
  ),
  CategoryIconModel(
      category: "Adventure",
      iconData: (Icons.flash_on_outlined),
      color: Colors.red),
];
