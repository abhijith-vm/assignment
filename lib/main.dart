import 'package:Assignment/core/theme/theme.dart';

import 'package:flutter/material.dart';

import 'components/home_tab.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: AppTheme.dark(),
      home: Hometab(),
    );
  }
}
