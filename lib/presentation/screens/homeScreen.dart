import 'package:Assignment/components/category.dart';

import 'package:Assignment/components/category_icon.dart';

import 'package:Assignment/components/popular_games.dart';
import 'package:Assignment/components/today_games.dart';
import 'package:Assignment/core/assets.dart';
import 'package:Assignment/core/colorPalette.dart';
import 'package:Assignment/model/category_icon_model.dart';
import 'package:Assignment/model/category_model.dart';
import 'package:Assignment/model/game_model.dart';
import 'package:Assignment/model/sorting_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'dart:ui';
import 'package:flutter/cupertino.dart';

class HomePageScreen extends StatefulWidget {
  HomePageScreen({Key key}) : super(key: key);

  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  Icon actionIcon = new Icon(Icons.search);
  bool category = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CircleAvatar(
              child: Image(image: AssetImage(Assets.PERSON)),
              radius: 18,
              backgroundColor: Palette.SECONDARY,
            ),
            SizedBox(
              width: 8,
            ),
            Text('Jhon'),
          ],
        ),
        actions: [
          new IconButton(
            icon: actionIcon,
            iconSize: 28,
            onPressed: () {},
          ),
          new IconButton(
              icon: Icon(
                Icons.notifications_outlined,
                size: 28,
              ),
              onPressed: () {}),
        ],
      ),
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Divider(
                  color: Palette.Grey,
                  thickness: 1,
                ),
                Text(
                  'Popular',
                  style: TextStyle(
                    color: Palette.SECONDARY,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                buildPopular(),
                Divider(
                  color: Palette.Grey,
                  thickness: 1,
                ),
                buildCategory(),
                SizedBox(
                  height: 12,
                ),
                buildCategoryIcon(),
                SizedBox(
                  height: 12,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Today',
                    style: TextStyle(
                      color: Palette.SECONDARY,
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                buildToday(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'This week',
                    style: TextStyle(
                      color: Palette.SECONDARY,
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                buildToday(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildPopular() {
    final popularList = gameModel;
    int popularCount = popularList.length;

    return Container(
      height: 256,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: popularCount,
          itemBuilder: (BuildContext ctxt, int i) {
            return GamesCard(
              gameModel: popularList[i],
            );
          }),
    );
  }

  Widget buildToday() {
    // final todayList = gameModel;
    // int todayCount = todayList.length;
    // final sortingList = sortingModel;
    return Column(
      children: [
        TodayGamesCard(
          prflImg: Assets.FARCRY,
          name: "Farcry",
        ),
        TodayGamesCard(
          prflImg: Assets.RIDERS,
          name: "Rders Republic",
        )
      ],
    );
  }

  Widget buildCategory() {
    final categoryList = categoryModel;
    int indexNumCat = categoryList.length;
    return Container(
      height: 28,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: indexNumCat,
          itemBuilder: (BuildContext ctxt, int i) {
            return CustomCategoryButton(
              categorymodel: categoryList[i],
            );
          }),
    );
  }

  Widget buildCategoryIcon() {
    final categoryIconList = categoryIconModel;
    int indexNumSort = categoryIconList.length;
    return Column(
      children: [
        Container(
          height: 38,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: indexNumSort,
              itemBuilder: (BuildContext ctxt, int i) {
                return CategoryIconButton(
                  categoryIconModel: categoryIconList[i],
                );
              }),
        ),
        SizedBox(
          height: 12,
        )
      ],
    );
  }
}
